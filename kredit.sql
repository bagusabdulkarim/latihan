-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Mar 2017 pada 09.18
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kredit`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `manager`
--

CREATE TABLE IF NOT EXISTS `manager` (
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `manager`
--

INSERT INTO `manager` (`username`, `password`) VALUES
('manager', '1d0258c2440a8d19e716292b231e3190');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas1`
--

CREATE TABLE IF NOT EXISTS `petugas1` (
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas1`
--

INSERT INTO `petugas1` (`username`, `password`) VALUES
('bagus', '17b38fc02fd7e92f3edeb6318e3066d8');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas2`
--

CREATE TABLE IF NOT EXISTS `petugas2` (
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas2`
--

INSERT INTO `petugas2` (`username`, `password`) VALUES
('2', 'c81e728d9d4c2f636f067f89cc14862c'),
('abdul', '82027888c5bb8fc395411cb6804a066c'),
('petugas2', 'ac5604a8b8504d4ff5b842480df02e91');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` varchar(10) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Country` varchar(40) NOT NULL,
  `penghasilan` int(13) NOT NULL,
  `email` varchar(30) NOT NULL,
  `petugas1` varchar(25) NOT NULL,
  `petugas2` varchar(25) NOT NULL,
  `manager` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`ID`, `Name`, `City`, `Country`, `penghasilan`, `email`, `petugas1`, `petugas2`, `manager`) VALUES
('k01', 'Alfreds Futterkiste', 'Berlin', 'Germany', 3000000, 'AlfredsFutterkiste@gmail.com', 'DITERIMA', 'DITOLAK', ''),
('k02', 'Ana Trujillo Emparedados y helados', 'México D.F.', 'Mexico', 2000000, 'AnaTrujillo@gmail.com', 'DITOLAK', '', ''),
('k03', 'Antonio Moreno Taquería', 'México D.F.', 'Mexico', 4000000, 'AntonioMoreno@gmailcom', 'DITERIMA', 'DITERIMA', 'DITERIMA'),
('k04', 'Around the Horn ', 'London', 'UK', 30000000, 'Around@gmail.com', 'DITERIMA', '', ''),
('k05', 'B''s Beverages', 'London', 'UK', 10000000, 'Beverages@gmail.com', 'DITERIMA', '', ''),
('k06', 'Berglunds snabbköp', 'Luleå ', 'Sweden', 300000, 'Berglunds@gmail.com', 'DITERIMA', 'DITOLAK', ''),
('k07', 'Blauer See Delikatessen ', 'Mannheim', 'Germany', 20000000, 'Blauer@gmail.com', 'DITERIMA', '', ''),
('k08', 'Blondel père et fils', 'Strasbourg', 'France', 1000000, 'Blondel', '', '', ''),
('k09', 'Bólido Comidas preparadas', 'Madrid', 'Spain', 5000000, 'Comidas@gmail.com', 'DITOLAK', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `petugas1`
--
ALTER TABLE `petugas1`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `petugas2`
--
ALTER TABLE `petugas2`
 ADD PRIMARY KEY (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
