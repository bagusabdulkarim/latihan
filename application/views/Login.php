    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Latihan Uji Php</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/style.css">
  </head>
  <body>
    <!--header-->
    <header class="header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                            <span class="fa fa-bars"></span>
                        </button>
                            <a href="index.html" class="navbar-brand">LOGO</a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pengajuan Kredit<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php  echo base_url()."index.php/welcome/index";?>">daftar ajuan kredit</a></li>
                                        <li><a href="<?php  echo base_url()."index.php/welcome/index";?>">form ajuan krefit</a></li>
                                    </ul>
                                    </li>
                                <li><a href="<?php  echo base_url()."index.php/welcome/Login";?>">Login</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="wrapper wow fadeInUp delay-05s" >
               </div>
            
        </div>
    </header>
    <!--/ header-->
   
    <section class="section-padding wow fadeInUp delay-02s" id="fromAjuan">
        <div class="container">
            <div class="cta-info text-center">
                <form action="<?php  echo base_url()."index.php/welcome/cek_login";?>" method="POST">
                <p><h1>LOGIN Administrasi</h1>
                <br><form method="post" action="<?php  echo base_url()."index.php/auth/cek_login/";?>" align>
                    <input class="masuk" type="text" autocomplete="off" placeholder="Username .." name="username"><br/>
                    <input class="masuk" type="password" autocomplete="off" placeholder="Password .." name="password"><br/>
                    <strong>Level User : </strong> 
                    <select name="user">
                      <option value="petugas1">Petugas 1</option>
                      <option value="petugas2">Petugas 2</option>
                      <option value="manager">Manager</option>
                      </select>    
                    <br>
                    <input id="tombol" type="submit" value="Login">
                </form>          
                </div>
            </div>
        </div>
    </section>
    <!---->
    <!---->
    
    <!---->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © latihan uji - All rights reserved
                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!---->
    
    <script src="<?php echo base_url()."aset/";?>js/jquery.min.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/wow.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/custom.js"></script>
    <script src="<?php echo base_url()."aset/";?>Peta Wilayahform/Peta Wilayahform.js"></script>
    
  </body>
</html>