    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Latihan Uji Php</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."aset/";?>css/style.css">
  </head>
  <body>
    <!--header-->
    <header class="header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                            <span class="fa fa-bars"></span>
                        </button>
                            <a href="index.html" class="navbar-brand">LOGO</a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php  echo base_url()."index.php/welcome/Logout";?>">logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="wrapper wow fadeInUp delay-05s" align="center" >
              
            <h2>Selamat Datang di Halaman Petugas 1</span></h2>
        </div>
    </header>
    <!--/ header-->
    <!---->
    <section class="section-padding wow fadeInUp delay-02s" id="DaftarAjuan">
        <div class="container">
         <div class="row">
                <div class="cta-info text-center">
                <h1><solid>Pengajuan kredit</solid></h1>
                    <table border="1" style="border-collapse:collapse; width:100%;">
                    <tr>
                        <th>ID</th>
                        <th >Nama</th>
                        <th>Kota</th>
                        <th>Negara</th>
                        <th>penghasilan</th>
                        <th>email</th>
                        <th>ACTION</th>
                        <?php foreach ($data as $r){ ?>
                        <tr>
                            <td><?php echo $r['ID'];      ?></td>
                            <td><?php echo $r['Name'];  ?></td>
                            <td ><?php echo $r['City'];      ?></td>
                            <td ><?php echo $r['Country'];        ?></td>
                            <td ><?php echo $r['penghasilan'];        ?></td>
                            <td ><?php echo $r['email'];        ?></td>
                            <td><table style="border-collapse:collapse; width:100%;">
							<tr>
							<td align ="center" ><a href="<?php  echo base_url()."index.php/welcome/p1terima/" . $r['ID'];?>" ><button >TERIMA</button></a></td>
							<td align ="center" ><a href="<?php  echo base_url()."index.php/welcome/p1tolak/" . $r['ID'];?>" ><button>TOLAK</button></a></td>
						</tr>
						</table>
						</td>
                        </tr>
                        <?php }?>
                    </table>
                </div>
            </div>
         </div>
    </section>
    <!---->
    
    
    
    <!---->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © latihan uji - All rights reserved
                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!---->
    
    <script src="<?php echo base_url()."aset/";?>js/jquery.min.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/wow.js"></script>
    <script src="<?php echo base_url()."aset/";?>js/custom.js"></script>
    <script src="<?php echo base_url()."aset/";?>Peta Wilayahform/Peta Wilayahform.js"></script>
    
  </body>
</html>