<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index(){
		$data = $this -> model_pengajuan -> getuser();
		$this->load->view('index',array('data' => $data));
	}
	public function do_insert(){
		$ID=$_POST['ID'];
		$Name=$_POST['Name'];
		$City=$_POST['City'];
		$Country=$_POST['Country'];
		$Penghasilan=$_POST['Penghasilan'];
		$email=$_POST['email'];
		$data_insert=array(
			'ID'=>$ID,
			'Name'=>$Name,
			'City'=>$City,
			'Country'=>$Country,
			'Penghasilan'=>$Penghasilan,
			'email'=>$email
		);
		if (empty($_POST['ID']) or empty($_POST['Name'])or empty($_POST['City'])or empty($_POST['Country']) or empty($_POST['Penghasilan'])or empty($_POST['email']) ){
		   echo "<script>alert('Tolong Isi Data Dengan Lengkap!');history.go(-1);</script>";
		}
		else{
			$res=$this->model_pengajuan->InsertData('user',$data_insert);
			if($res>=1){
				echo "<script>alert('Data Berhasil Tersimpan!')</script>";
				$data = $this -> model_pengajuan -> getuser();
			$this->load->view('index',array('data' => $data));
			}else{
				echo "<script>alert('Data tidak Berhasil Tersimpan!');history.go(-1);</script>";
			}
		}
	}
	public function logout(){
	session_start();
	session_destroy();
	redirect('welcome/index');
	}

	public function login(){
		
		$this->load->view('login');
	}
	public function p1terima($ID){
		$data_terima=array(
		'petugas1'=>"DITERIMA"
		);
		$where=array('ID'=>$ID);
		$res=$this->model_pengajuan->p1terima('user',$data_terima,$where);
		if($res>=1){
			echo "<script>alert('data diterima!')</script>";
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('petugas1',array('data' => $data));
				}else{
					echo "<script>alert('Tidak Diterima);history.go(-1);</script>";
				
			}
	}
	public function p1tolak($ID){
		$data=array(
		'petugas1'=>"DITOLAK"
		);
		$where=array('ID'=>$ID);
		$res=$this->model_pengajuan->p1terima('user',$data,$where);
		if($res>=1){
			echo "<script>alert('data telah ditolak!')</script>";
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('petugas1',array('data' => $data));
				}else{
					echo "<script>alert('Tidak Diterima);history.go(-1);</script>";
				
			}
	}
	public function p2terima($ID){
		$data_terima=array(
		'petugas2'=>"DITERIMA"
		);
		$where=array('ID'=>$ID);
		$res=$this->model_pengajuan->p2terima('user',$data_terima,$where);
		if($res>=1){
			echo "<script>alert('data diterima!')</script>";
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('petugas2',array('data' => $data));
				}else{
					echo "<script>alert('Tidak Diterima);history.go(-1);</script>";
				
			}
	}
	public function p2tolak($ID){
		$data=array(
		'petugas2'=>"DITOLAK"
		);
		$where=array('ID'=>$ID);
		$res=$this->model_pengajuan->p2terima('user',$data,$where);
		if($res>=1){
			echo "<script>alert('data telah ditolak!')</script>";
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('petugas2',array('data' => $data));
				}else{
					echo "<script>alert('Tidak Diterima);history.go(-1);</script>";
				
			}
	}
	public function Mterima($ID){
		$data_terima=array(
		'manager'=>"DITERIMA"
		);
		$where=array('ID'=>$ID);
		$res=$this->model_pengajuan->Mterima('user',$data_terima,$where);
		if($res>=1){
			echo "<script>alert('data diterima!')</script>";
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('manager',array('data' => $data));
				}else{
					echo "<script>alert('Tidak Diterima);history.go(-1);</script>";
				
			}
	}
	public function Mtolak($ID){
		$data=array(
		'manager'=>"DITOLAK"
		);
		$where=array('ID'=>$ID);
		$res=$this->model_pengajuan->Mterima('user',$data,$where);
		if($res>=1){
			echo "<script>alert('data telah ditolak!')</script>";
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('manager',array('data' => $data));
				}else{
					echo "<script>alert('Tidak Diterima);history.go(-1);</script>";
				
			}
	}


	public function cek_login() {
		$user=$_POST['user'];
		$data = array('username' => $this->input->post('username', TRUE),
				'password' => md5($this->input->post('password', TRUE)));
		if ($user=="petugas1"){
			
			$hasil = $this->model_pengajuan->petugas1($data);
			if ($hasil->num_rows() == 1) {	
			$d = $this -> model_pengajuan -> getuser();
			$this->load->view('petugas1',array('data' => $d));
			}
			else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
			}
		}
		if ($user=="petugas2"){
			
			$hasil = $this->model_pengajuan->petugas2($data);
			if ($hasil->num_rows() == 1) {	
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('petugas2',array('data' => $data));
			}
			else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
			}
		}
		if ($user=="manager"){
			
			$hasil = $this->model_pengajuan->manager($data);
			if ($hasil->num_rows() == 1) {	
			$data = $this -> model_pengajuan -> getuser();
			$this->load->view('manager',array('data' => $data));
			}
			else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
			}
		}
	}		
}