<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pengajuan extends CI_Model {
	function getuser(){
		$data = $this -> db -> query('select * from user');		
		return $data->result_array();
	}
	public function InsertData($tabelName,$data){
		$res=$this->db->insert($tabelName,$data);
		return $res;
	}
	public function petugas1($data) {
			$query = $this->db->get_where('petugas1', $data);
			return $query;
		}
	public function manager($data) {
			$query = $this->db->get_where('manager', $data);
			return $query;
		}
	public function petugas2($data) {
			$query = $this->db->get_where('petugas2', $data);
			return $query;
		}
	public function p1terima ($tabelName,$data,$where){
		$res=$this->db->update($tabelName,$data,$where);
		return $res;
	}
	public function p2terima ($tabelName,$data,$where){
		$res=$this->db->update($tabelName,$data,$where);
		return $res;
	}
	public function Mterima ($tabelName,$data,$where){
		$res=$this->db->update($tabelName,$data,$where);
		return $res;
	}
}
